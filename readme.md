## Background

This document explains how to extract the benign traces using the gem5 simulator [1] from SPEC CPU®2017 [2] benchmark packages. 


## Introduction to gem5 simulator

The gem5 simulator ("gem5") is a modular platform for computer-system architecture research, encompassing system-level architecture as well as process microarchitecture [1]. Since the original paper has published in 2011 [3], it has been widely used both in academia and industry. 
To install gem5, please refer to https://www.gem5.org/documentation/learning_gem5/part1/building/ for further information.


## Introduction to SPEC CPU®2017

The SPEC CPU®2017 ("SPEC") is designed to provide performance measurements that can be used to compare compute-intensive workloads on different computer systems. SPEC contains 43 benchmarks organized into four suites: SPECspeed 2017 Integer, SPECspeed 2017 Floating Point, SPECrate 2017 Integer, and SPECrate 2017 Floating Point [4]. For installation, refer to https://www.spec.org/cpu2017/Docs/install-guide-unix.html.

For our paper, we have used 10 benchmarks as below:

|No|Suite| Benchmark|
|:--:|:--|:--|
|1 |SPECrate2017 Integer | 500.perlbench_r |
|2 |SPECrate2017 Integer | 502.gcc_r |
|3 |SPECrate2017 Integer | 505.mcf_r |
|4 |SPECrate2017 Integer | 548.exchange2_r |
|5 |SPECrate2017 Floating Point | 549.fotonik3d_r |
|6 |SPECspeed2017 Integer | 602.gcc_s |
|7 |SPECspeed2017 Floating Point | 607.cactuBSSN_s |
|8 |SPECspeed2017 Integer | 631.deepsjeng_s |
|9 |SPECspeed2017 Floating Point | 638.imagick_s |
|10 |SPECspeed2017 Integer | 641.leela_s |

Note: After extracted and examined the trace files, `602.gcc_s` has similar memory access patterns like `502.gcc_r`, thus not used to when generating combinations with other SPEC programs. 


## Preparation

1. Both gem5 and SPEC CPU 2017 and their dependencies are already installed.
2. Below is the version information of the configuration we have used.

    1. gem5: 21.2.1.1  (commit ID: e4fae58da6c044b6efec62392ff99f343ce67947)
    2. SPEC CPU2017: 1.0.1
    3. GCC (for SPEC compilation): 6.3.0
    4. OS: Ubuntu 20.04

3. Storage capacity for 200GB. 
 

## Step 1. Compile the SPEC

First, we need to prepare the SPEC configurations to run the benchmarks. The SPEC provides sample compilers readily available. 
Config files contain details such as portability and optimization options. However, we need to modify some lines before use.

Check the below path to find a config file `Example-gcc-linux-x86.cfg`. You may copy that file with the shorter name, i.e., `try1.cfg`.

    ~/cpu2017/config

Then modify L214 of `try1.cfg` as below and save it:
    
    OPTIMIZE       = -g -O3 -march=x86-64 -fno-unsafe-math-optimizations  -fno-tree-loop-vectorize

NOTE: older gcc might not support some of the optimization switches. When using `502.gcc_r` or `602.gcc_s`, change the OPTIMIZE option in L214 as below:

    OPTIMIZE       = -g -O3 -march=x86-64 -fno-unsafe-math-optimizations  -fno-tree-loop-vectorize  -fno-strict-aliasing

For `500.perlbench_r`, us below flag for OPTIMIZE:

    OPTIMIZE       = -g -O3 -march=x86-64 -fno-unsafe-math-optimizations  -fno-tree-loop-vectorize  -fgnu89-inline

We use the `505.mcf_r` benchmark for the sample explanations [5]. 
Below commands will get you to the path for `505.mcf_r`.

Go to the below SPEC path, and set up the environment using `shrc`. 

    ~/cpu2017$ source shrc # set up the SPEC environment
    ~/cpu2017$ go 505 # provides a shortcut to the path of the benchmark

Now we proceed with a fake run. First, remove the existing build (if run before), then start it over.

    ~/cpu2017/benchspec/CPU/505.mcf_r$ rm -r build
    ~/cpu2017/benchspec/CPU/505.mcf_r$ runcpu --fake --config try1 505

Above command has created the `build` and `run` folders under the current path. 
When successful, it will display the below messages at the end.

    Success: 1x505.mcf_r
    The log for this run is in ~/cpu2017/result/CPU2017.xxx.log 

    runcpu finished at 20xx-xx-xx 18:30:51; x total seconds elapsed

Now let's compile the SPEC. We need to designate the compiler location to build.

    ~/cpu2017/benchspec/CPU/505.mcf_r$ cd build/build_base_mytest-m64.0000
    ~/cpu2017/benchspec/CPU/505.mcf_r/build/build_base_mytest-m64.0000$ which gcc 

    /usr/bin/gcc # your path to the compiler will appear

    ~/cpu2017/benchspec/CPU/505.mcf_r/build/build_base_mytest-m64.0000$ specmake SPECLANG=/usr/bin/

This command compiles then generates a binary after the name of a benchmark (in this case, `mcf_r`) in the current folder.
Repeat for other benchmarks.

## Step 2. Get an argument for gem5

If you do not want to get an argument individually, skip this step and proceed to next step.

We need to get arguments to run the SPEC binary. First, let's move to the `run` directory.

    ~/cpu2017/benchspec/CPU/505.mcf_r/build/build_base_mytest-m64.0000$ go 505
    ~/cpu2017/benchspec/CPU/505.mcf_r$ cd run/run_base_refspeed_mytest-m64.0000/
    ~/cpu2017/benchspec/CPU/505.mcf_r/run/run_base_refspeed_mytest-m64.0000$ specinvoke -n

    # specinvoke r4356
    #  Invoked as: specinvoke -n
    # timer ticks over every 1000 ns
    # Use another -n on the command line to see chdir commands and env dump
    # Starting run for copy #0
    ../run_base_refrate_mytest-m64.0000/mcf_r_base.mytest-m64 inp.in  > inp.out 2>> inp.err
    specinvoke exit: rc=0

Here, we get the command line argument to run the `505.mcf_r` benchmark.

    inp.in

Now you can simulate the benchmark in gem5 using below command. below command initiates `505.mcf_r` benchmark using `AtomicSimpleCPU`. Adjust the path to save a M5.out file and read the SPEC binary according to your environment.

    $ ~/gem5/build/X86/gem5.opt \
    --debug-flags=MemoryAccess \
    --debug-file=/hdd/505-M5.out \
    ~/gem5/configs/example/se.py \
    --cmd=/home/lee/Desktop/cpu2017/benchspec/CPU/505.mcf_r/build/build_base_mytest-m64.0000/mcf_r \
    --options="inp.in" \
    --mem-size=8GB


## Step 3. Running benchmark simulation using gem5

We have prepared a shell script `runspec_MACTA.sh` to get a memory trace log from each benchmark for gem5 in an automated manner. Save the file in the below path:

    ~/gem5/runspec_MACTA.sh

Execute the file with the number in the benchmark name as an argument. 

    ./runspec_MACTA.sh 505

This command will begin the gem5 simulation and save a log file in the designated path in the `runspec_MACTA.sh` file. Again, you need to modify the path according to your working environment. Please take a look at the last part of `runspec_MACTA.sh` file.

Once the file size reaches adequate size, you can kill the process. The file size of around 15GB is large enough to extract the traces.  

## Step 4. Generate mixes

The attached `trace_converter.py`  file will extract, modify and mix the traces from SPEC benchmarks. For example, you can specify the argument as example: 

    python traces_converter.py 502 4 505 6

 - 1st argument: first input file name
 - 2nd argument: skipping the first 4 million memory accesses from the first input file
 - 3rd argument: second input file name
 - 4th argument: skipping the first 6 million memory accesses from the second input file

To extract traces simultaneouysly as used in the experiment, execute the attached `generate_loop.py`, without any arguments. both python files should be located in the same directory where your gem5 simulation results are located.

complete size of each combination file is around 140 MB.

## Research paper
The paper is available in the procceedings of the Eleventh International Conference on Learning Representations (ICLR 2023).
Please cite our work with the bibtex entry below:

    @inproceedings{
    cui2023macta,
    title={{MACTA}: A Multi-agent Reinforcement Learning Approach for Cache Timing Attacks and Detection},
    author={Jiaxun Cui and Xiaomeng Yang and Mulong Luo and Geunbae Lee and Peter Stone and Hsien-Hsin S. Lee and Benjamin Lee and G. Edward Suh and Wenjie Xiong and Yuandong Tian},
    booktitle={The Eleventh International Conference on Learning Representations },
    year={2023},
    url={https://openreview.net/forum?id=CDlHZ78-Xzi}
    }

## References

1. gem5 Documentation https://www.gem5.org/documentation/
2. SPEC CPU 2017 Documentation https://www.spec.org/cpu2017/Docs/
3. The gem5 Simulator. Nathan Binkert, Bradford Beckmann, Gabriel Black, Steven K. Reinhardt, Ali Saidi, Arkaprava Basu, Joel Hestness, Derek R. Hower, Tushar Krishna, 
   Somayeh Sardashti, Rathijit Sen, Korey Sewell, Muhammad Shoaib, Nilay Vaish, Mark D. Hill, and David A. Wood. May 2011, ACM SIGARCH Computer Architecture News. 
   doi:10.1145/2024716.2024718.
4. SPEC CPU®2017 https://www.spec.org/cpu2017/
5. Lecture note CS251a from UCLA explain how to simulate SPEC CPU 2017 benchmark on gem5 https://polyarch.github.io/cs251a/resources/spec2017-gem5/


