""" This file will extract, modify and mix the traces from SPEC benchmarks
    Copyright (C) 2023 Vishnu Kumar Kalidasan (kvishnukumar22@vt.edu)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""

import csv, sys, os, getopt
import subprocess

def parse_line(line):
    
    try:
        if (len(line.split()) != 14) or not (("data" in line) and ("address" in line)):
            print("irregular line found :", line)
            return (0, None)
        parts = line.split()
        time = parts[0][0:-1]
        operation = parts[2].replace('Read', 'R').replace('Write', 'W')
        size = parts[7]
        address = parts[-4]
        if address == 'on':
            print(line)
        assert(address != 'on')
        data = parts[-2]
        
        return (int(time), f"{operation} {size} {address} {data}")
        
    except:
        print("parsing exception occured", exception)
        return (0, None)

def check_skip(line, inF, keys):

    while (line and not (("Read from" in line and " data " in line) or ("Write from" in line and ' data ' in line))):
        line = inF.readline()
    return line


def skip_read_write(line, inF, skip_count):
    for itr in range(0, skip_count):
        while (line and not (("Read from" in line and " data " in line) or ("Write from" in line and " data " in line))):
            line = inF.readline()
        
        line = inF.readline()

    return line
def merge_and_sort_files(file1, file2, outf, spec_no1, spec_no2, skip_count1, skip_count2, sub_id1, sub_id2):
    with open(file1, 'r') as f1:
        with open(file2, 'r') as f2:
            time1_off = time2_off = 0
            o1 = open(outf, 'w+')
            LINE_LIMIT = 4000000
            f_count = 0
            no_exit = 1
            line1 = f1.readline()
            line2 = f2.readline()

            time1 = time2 = 0
            while (line1 and (line1.strip().startswith("0:") )): 
                line1 = f1.readline()
            while (line2 and (line2.strip().startswith("0:") )): 
                line2 = f2.readline()
            line1 = skip_read_write(line1, f1, skip_count1)
            line2 = skip_read_write(line2, f2, skip_count2)
 
            while line1 and line2:
 
                while (line1 and not (("Read from" in line1 and ' data ' in line1) or ("Write from" in line1 and ' data ' in line1))):
                    line1 = f1.readline()
                    
                while (line2 and not (("Read from" in line2 and 'data' in line2) or ("Write from" in line2 and 'data' in line2))):
                    line2 = f2.readline()
                
                if not line1:
                    break
                if not line2:
                    break
                time1, ret1 = parse_line(line1)
                time2, ret2 = parse_line(line2)
                if time1_off == 0:
                    time1_off = time1
                time1 = time1 - time1_off
                if time2_off == 0:
                    time2_off = time2
                time2 = time2 - time2_off
          
                while(line2 and (time1 > time2) and (f_count <= LINE_LIMIT)):
                    if ret2:
                        o1.write(str(sub_id2)+spec_no2+" "+ret2+" "+str(time2)+"\n")
                        f_count+=1
                 
                    line2 = f2.readline()
                    while (line2 and not (("Read from" in line2 and ' data ' in line2) or ("Write from" in line2 and ' data ' in line2))):
                        line2 = f2.readline()
                    if not line2:
                        break
                    time2, ret2 = parse_line(line2)
                    time2 = time2 - time2_off
                 
                if f_count >= LINE_LIMIT:
                    break
                if ret1:
                    o1.write(str(sub_id1)+spec_no1+" "+ret1+" "+str(time1)+"\n")
                    f_count+=1

                line1 = f1.readline()
                
                if not line1:
                    no_exit = 0
                
            while line1:
                while (line1 and not (("Read from" in line1 and ' data ' in line1) or ("Write from" in line1 and ' data ' in line1))):
                    line1 = f1.readline()
                if not line1:
                    break
                time1, ret1 = parse_line(line1)
                if time1_off == 0:
                    time1_off = time1
                time1 = time1 - time1_off
                if f_count > LINE_LIMIT:
                    break
                
                if ret1:
                    o1.write(str(sub_id1)+spec_no1+" "+ret1+" "+str(time1)+"\n")
                    
                    f_count+=1 
               
                line1 = f1.readline()

            while line2:
                while (line2 and not (("Read from" in line2 and ' data ' in line2) or ("Write from" in line2 and ' data ' in line2))):
                    line2 = f2.readline()
                if not line2:
                    break
                time2, ret2 = parse_line(line2)
                if time2_off == 0:
                    time2_off = time2
                time2 = time2 - time2_off
                if f_count > LINE_LIMIT:
                    break
                
                if ret2:
                    o1.write(str(sub_id2)+spec_no2+" "+ret2+" "+str(time2)+"\n")
                    f_count+=1
                
                line2 = f2.readline()
            o1.close()
            f2.close()
        f1.close()
    print ("exited")
    return 0

def main(argv): 
    if len(argv) < 4:
        print("usage format:\n\t python <script.py> spec#1 skip_count#1 spec#2 skip_count#2 ")
        print("for skip count:\n\t choose one number from 2, 4, 6, 8 to 10. each number represents in million lines to skip ")
        return
    skip_count1 = 0
    skip_count2 = 0
    spec_no1 = argv[0]
    
    num1 = int(argv[1])
    skip_count1 = num1 * 1000000
    sub_id1 = int(skip_count1 / 10000)

    spec_no2 = argv[2]
    num2 = int(argv[3])
    
    skip_count2 = num2 * 1000000
    sub_id2 = int(skip_count2 / 10000)
  
    file_name1 = spec_no1 + '-M5.out'
    file_name2 = spec_no2 + '-M5.out'
    
    outfile = spec_no1 + "-" + str(argv[1]) + "M" + "_" + spec_no2 + "-" + str(argv[3]) + "M" + ".txt"
    if not os.path.exists(file_name1):
        print("invalid input file ", file_name1)
        return
    
    if not os.path.exists(file_name2):
        print("invalid input file ", file_name2)
        return
    print("found files ")
    print (file_name1, " ", file_name2)

    print("writing to merged file ", outfile)
   
    ret = merge_and_sort_files(file_name1, file_name2, outfile, spec_no1, spec_no2, skip_count1, skip_count2, sub_id1, sub_id2)

    if (ret == 0):
        print("success")
    else:
        print("failed")

    with open(outfile, 'r') as f:
        lines = f.readlines()
        last_10_lines = lines[-10:]

        for line in last_10_lines:
            print(line)

if __name__ == '__main__':
    main(sys.argv[1:])
