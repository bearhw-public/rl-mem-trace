#  Get a memory trace log from each benchmark for gem5 in an automated manner.
#  The contents of this script are based on this blog: 
#  https://markgottscho.wordpress.com/2014/09/20/tutorial-easily-running-spec-cpu2006-benchmarks-in-the-gem5-simulator/
#  Copyright (C) 2023 Vishnu Kumar Kalidasan (kvishnukumar22@vt.edu), John Lee (geunbae@vt.edu)

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>."""



BENCHMARK=$1                    
BENCHMARK_CODE="none"
BENCHMARK_OPTION=""
BENCHMARK_PATH=""

if [[ "$BENCHMARK" == "500" ]]; then 
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/500.perlbench_r/build/build_base_mytest-m64.0000/perlbench_r
     BENCHMARK_OPTION="-I./lib splitmail.pl 6400 12 26 16 100 0"
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/500.perlbench_r/run/run_base_refrate_mytest-m64.0000
fi

if [[ "$BENCHMARK" == "502" ]]; then 
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/502.gcc_r/build/build_base_mytest-m64.0000/cpugcc_r
     BENCHMARK_OPTION="ref32.c -O3 -fselective-scheduling -fselective-scheduling2 -o ref32.opts-O3_-fselective-scheduling_-fselective-scheduling2.s"
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/502.gcc_r/run/run_base_refrate_mytest-m64.0000
fi

if [[ "$BENCHMARK" == "505" ]]; then
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/505.mcf_r/build/build_base_mytest-m64.0000/mcf_r
     BENCHMARK_OPTION="inp.in"
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/505.mcf_r/run/run_base_refrate_mytest-m64.0000
fi

if [[ "$BENCHMARK" == "548" ]]; then
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/548.exchange2_r/build/build_base_mytest-m64.0000/exchange2_r
     BENCHMARK_OPTION="6"
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/548.exchange2_r/run/run_base_refrate_mytest-m64.0000
fi

if [[ "$BENCHMARK" == "549" ]]; then
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/549.fotonik3d_r/build/build_base_mytest-m64.0000/fotonik3d_r
     BENCHMARK_OPTION=""
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/549.fotonik3d_r/run/run_base_refrate_mytest-m64.0000
fi

if [[ "$BENCHMARK" == "602" ]]; then 
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/602.gcc_s/build/build_base_mytest-m64.0000/sgcc
     BENCHMARK_OPTION="gcc-pp.c -O5 -finline-limit=24000 -fgcse -fgcse-las -fgcse-lm -fgcse-sm -o gcc-pp.opts-O5_-finline-limit_24000_-fgcse_-fgcse-las_-fgcse-lm_-fgcse-sm.s"
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/602.gcc_s/run/run_base_refspeed_mytest-m64.0000
fi

if [[ "$BENCHMARK" == "607" ]]; then
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/607.cactuBSSN_s/build/build_base_mytest-m64.0000/cactuBSSN_s
     BENCHMARK_OPTION="spec_ref.par"
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/607.cactuBSSN_s/run/run_base_refspeed_mytest-m64.0000
fi

if [[ "$BENCHMARK" == "631" ]]; then
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/631.deepsjeng_s/build/build_base_mytest-m64.0000/deepsjeng_s     
     BENCHMARK_OPTION="ref.txt"
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/631.deepsjeng_s/run
fi

if [[ "$BENCHMARK" == "638" ]]; then
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/638.imagick_s/build/build_base_mytest-m64.0000/imagick_s
     BENCHMARK_OPTION="-limit disk 0 refspeed_input.tga -resize 817% -rotate -2.76 -shave 540x375 -alpha remove -auto-level -contrast-stretch 1x1% -colorspace Lab -channel R -equalize +channel -colorspace sRGB -define histogram:unique-colors=false -adaptive-blur 0x5 -despeckle -auto-gamma -adaptive-sharpen 55 -enhance -brightness-contrast 10x10 -resize 30% refspeed_output.tga"
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/638.imagick_s/run/run_base_refspeed_mytest-m64.0000
fi

if [[ "$BENCHMARK" == "641" ]]; then
     BENCHMARK_CODE=~/Desktop/cpu2017/benchspec/CPU/641.leela_s/build/build_base_mytest-m64.0000/leela_s
     BENCHMARK_OPTION="ref.sgf"
     BENCHMARK_PATH=~/Desktop/cpu2017/benchspec/CPU/641.leela_s/run/run_base_refspeed_mytest-m64.0000
fi

RUN_DIR=$BENCHMARK_PATH
SCRIPT_OUT=$BENCHMARK.log
M5OUT=$BENCHMARK-M5.log
cd $RUN_DIR
 
 ~/gem5/build/X86/gem5.opt \
 --debug-flags=MemoryAccess \
 --debug-file=/hdd/test/$BENCHMARK-M5.out \
 ~/gem5/configs/example/se.py \
 --cmd=$BENCHMARK_CODE \
 --options="$BENCHMARK_OPTION" \
 --mem-size=8GB > $M5OUT
