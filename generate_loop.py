""" This file generates mix files used in the paper
    Copyright (C) 2023 Vishnu Kumar Kalidasan (kvishnukumar22@vt.edu), John Lee (geunbae@vt.edu)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""
    
import subprocess

''' combinations in Figure 6 in Appendix A.3 '''
combinations = [(500, 2, 500, 4), (502, 2, 502, 4), (505, 2, 505, 4), (500, 2, 502, 2), 
                (500, 2, 505, 2), (502, 2, 505, 2), (500, 6, 502, 6), (500, 6, 505, 6), 
                (502, 6, 505, 6), (500, 10, 502, 10), (500, 10, 505, 10), (502, 10, 505, 10), 
                (549, 2, 607, 2), (548, 2, 548, 4), (631, 2, 631, 4), (638, 2, 638, 4), 
                (641, 2, 641, 4), (548, 2, 631, 2), (548, 2, 638, 2), (548, 2, 641, 2), 
                (631, 2, 638, 2), (631, 2, 641, 2), (638, 2, 641, 2)]

for args in combinations:

    command = f"python trace_converter.py {args[0]} {args[1]} {args[2]} {args[3]}"
    print(command)
    subprocess.run(command, shell=True)

        
